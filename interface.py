from turtle import*
from random import*

setup(900,550)

screen = Screen()
screen.tracer(0)

colors=['blue','green','black','white','pink','brown','blue','red','purple']

hauteur=window_height()
larg=window_width()

def rectangle(xr,yr,remplir,taille,longueur,largeur,couleur):
    up()
    home()
    goto(xr,yr)
    pensize(taille)
    down()
    if remplir==1:
        begin_fill()
    i=0
    while i<=1:
        forward(longueur)
        right(90)
        forward(largeur)
        right(90)
        i=i+1
    color(couleur)
    if remplir==1:
        end_fill()


def cercle(xc,yc,remplir,rotation,taille,rayon,entier,couleur):
    up()
    home()
    right(rotation)
    goto(xc,yc)
    pensize(taille)
    down()
    color(couleur)
    if remplir==1:
        begin_fill()
    circle(rayon,entier)
    if remplir==1:
        end_fill()


def dessinePolygone(nbCoté,remplir,loCoté,pointx,pointy,couleur):
    up()
    goto(pointx,pointy)
    down()
    color(couleur)
    if remplir==1:
        begin_fill()
    i=0
    while i<=nbCoté:
        forward(loCoté)
        left(360/nbCoté)
        i=i+1
    if remplir==1:
        end_fill()

def trapezeIsocele(xd,yd,xv1,yv1,v1,v2,remplir,couleur):
    up()
    home()
    pensize(2)
    color(couleur)
    if remplir==1:
        begin_fill()
    goto(xd,yd)
    xd1=xd+xv1
    yd1=yd+yv1
    down()
    goto(xd1,yd1)
    forward(v1)
    goto(xd*-1,yd)
    right(180)
    forward(v2)
    if remplir==1:
        end_fill()

def ligne(x1,y1,x2,y2,taille,couleur):
    up()
    goto(x1,y1)
    pensize(taille)
    color(couleur)
    down()
    goto(x2,y2)

def ligne2(x1,y1,long,a,taille,couleur):
    up()
    home()
    color(couleur)
    pensize(taille)
    right(a)
    goto(x1,y1)
    down()
    forward(long)


def siege():
    x=-1/2*larg+15
    y=180
    while y>50:
        while x<1/2*larg-15:
            color('yellow')
            rectangle(x,y,1,1,24,24,'yellow')
            cercle(x,y,1,90,1,12,-180,'yellow')
            x=x+60
        y=y-45
        x=-1/2*larg+15

def ligneTribune():
    y=200
    x=-1/2*larg
    while y>80:
        y=y-45
        print(ligne2(x,y,larg,0,2,(0.2,0.2,0.2)))

def cage():
    pencolor('grey')
    print(rectangle(-75,0,1,4,150,-50,(0.8,0.8,0.8)))

def spectateur(xs):
    a=0
    ligne2(xs,85,17,90,8,"blue")
    cercle(xs,85,1,0,1,8,360,(0.9,0.6,0.6))
    ligne2(xs-3,90,5,0,2,'black')
    pencolor('black')
    pensize(3)
    up()
    goto(xs+3,95)
    down()
    forward(1)
    up()
    goto(xs-3,95)
    down()
    forward(1)
    up()
    a=int(xs)
    
    return a

def specTombe(x):
    a=0
    j=0
    while j<n:
        ligne2(x,76,8,90,8,"blue")
        cercle(x,76,1,0,1,8,360,(0.9,0.6,0.6))
        ligne2(x,98,5,90,4,(0.8,0.5,0.5))
        ligne2(x-3,81,5,0,1,'black')
        pencolor('black')
        pensize(1)
        up()
        goto(x+4,86)
        down()
        forward(3)
        up()
        goto(x-4,86)
        down()
        forward(3)
        up()
        a=int(x)
        x=x+60
        j=j+1

    return a
        

def joueur(x,y):
    ligne2(x+25,y-173,170,0,9,(0,0.3,0))
    cercle(x,y,1,90,1,40,360,'black')
    cercle(x+1,y+6,1,180,1,8,180,(0.9,0.6,0.6))
    cercle(x+80,y-9,1,0,1,8,180,(0.9,0.6,0.6))
    rectangle(x+25,y-39,1,1,30,15,(0.9,0.6,0.6))
    ligne2(x+30,y-150,17,90,12,(0.9,0.6,0.6))
    ligne2(x+50,y-150,17,90,12,(0.9,0.6,0.6))
    rectangle(x+20,y-54,1,1,40,95,'blue')
    ligne(x+20,y-60,x+9,y-110,12,'blue')
    ligne(x+60,y-60,x+72,y-110,12,'blue')
    cercle(x+2,y-110,1,135,1,7,250,(0.9,0.6,0.6))
    cercle(x+67,y-110,1,115,1,7,250,(0.9,0.6,0.6))
    ligne2(x+20,y-118,40,0,1,'black')
    ligne2(x+40,y-130,20,90,1,'black')
    cercle(x+38,y-177,1,-90,1,9,180,'red')
    cercle(x+59,y-177,1,-90,1,9,180,'red')
    up()
    setpos(x+25,y-100)
    color('black')
    down()
    write('10',font=('Arial',20,'bold'))

def ballon(x,y):
    ligne2(x,y+1,35,0,8,(0,0.3,0))
    cercle(x,y,1,0,1,20,360,'white')
    cercle(x-5,y+4,1,0,1,5,360,'black')
    cercle(x+11,y+10,1,0,1,6,360,'black')
    cercle(x-10,y+20,1,0,1,5,360,'black')
    cercle(x+6,y+27,1,0,1,5,360,'black')

def nuage(x,y):
    ligne2(x,y,200,0,25,'white')
    cercle(x+75,y,1,-90,1,35,180,'white')
    cercle(x+140,y,1,-90,1,45,180,'white')
    cercle(x+190,y,1,-90,1,35,180,'white')

print(rectangle(-1/2*larg,1/2*hauteur,1,1,larg,hauteur,(0,0.6,0)))
print(rectangle(-1/2*larg,1/2*hauteur,1,1,larg,1/2*hauteur,(0.5,0.8,0.9)))
print(rectangle(-1/2*larg,200,1,1,larg,200,(0.8,0.2,0.2)))
print(trapezeIsocele(-200,0,-40,-95,480,400,0,'white'))
print(cage())
print(ligne(-1/2*larg,0,larg,0,2,'white'))
print(siege())
print(ligneTribune())
print(joueur(-1/2*larg+250,-70))
print(ballon(0,-170))
print(nuage(-1/2*larg+45,1/2*hauteur-55))
print(nuage(1/2*larg-250,1/2*hauteur-55))

#n=randint(5,15)

#textinput('A vous de jouer','choisir entre 1 et 3')

def joueurJoue(q) :
    condition_i = len(q) + 1
    i= numinput("jeu","choisir une ligne : ", minval=1, maxval=None)

    while i not in range(condition_i) :
        setpos(50,400)
        write("Erreur ! ligne saisie invalide")
        i=numinput("jeu","choisir une ligne : ", minval=1, maxval=None)

    p = textinput("jeu","choisir une position (G, M, ou D) : ")
    while p not in "GMD" :
        write("Erreur ! positision invalide")
        p = textinput("jeu","choisir une position (G, M, ou D) : ")
        

    joueur = str(i) + ":" + p
    return joueur

def ordiJoue(q) :
    p = choice(["G","M","D"])
    i = randint(1, len(q))
    ordi = str(i) + ":" + p

    if p == "M" :
        write("L'ordinateur a joué au milieu sur la ligne i et a tiré 2 quilles")

    elif p == "G":
        write("L'ordinateur a joué à gauche sur la ligne i et a tiré 1 quille")
    else :
        write("L'ordinateur a joué à droite sur la ligne i et a tiré 1 quille")

    return ordi


def jouerMilieu (c, q) :
    ligne = int(c[0])
    a = ligne - 1
    nbQuilles = q[a][-1] + 1 - q[a][0]

    milieuPair = int(nbQuilles/2 - 2 + q[a][0])
    milieuImpair = int((nbQuilles + 1 )/2 - 2 + q[a][0])

    if nbQuilles % 2 == 0 :
        q[a][-1] = milieuPair
        if q[a][0] > q[a][1] :
            del q[a]
        else :
            q.insert(ligne, [milieuPair + 3, nbQuilles - 1 + q[a][0]])
    

    else :
        q[a][-1] = milieuImpair
        if q[a][0] > q[a][1] :
            del q[a]
        else :
            q.insert(ligne , [milieuImpair + 3, nbQuilles - 1 + q[a][0]])
     
    return q 
    

def jouerCote (c,q) :
    ligne = int(c[0])
    pos = c[2]
    a = ligne - 1
    
    if pos == "G" :
        q[a][0] += 1
        if q[a][0] > q[a][1] :
            del q[a]
        

    elif pos == "D" :
        q[a][-1] -= 1
        if q[a][0] > q[a][1] :
            del q[a]

    return q

def jouer(c,q) :

    if c[2] == "M" :
        jouerMilieu(c,q)
        

    elif c[2] == "G" or "D" :
        jouerCote(c,q)

    return q

def afficheQuilles(q, n):
    x=-1/2*larg+27
    xs=-1/2*larg+27
    quilles=list(range(specTombe(x)))
    nbListe=len(q)

    for ligne in q:
        for i in range(ligne[0], ligne[1]+1):
            quilles[i] = spectateur(xs)
            xs=xs+60
            
    for k in range (len(q)) :
        if q[k][0] > q[k][1] :
            nbListe = nbListe - 1
            
    write("Voici les quilles, il y a nbListe lignes")
    return quilles

n=9
q = [[0, n -1]]

print(afficheQuilles(q,n))

while len(q) != 0 :
    jouer(joueurJoue(q),q)
    print(rectangle(-1/2*larg,200,1,1,larg,200,(0.8,0.2,0.2)))
    print(siege())
    print(ligneTribune())
    print(cage())
    print(q)
    print(afficheQuilles(q,n))
    if len(q) == 0 :
        write("Victoire ! C'est gagné :) ")
        break
    else :
        jouer(ordiJoue(q),q)
        print(rectangle(-1/2*larg,200,1,1,larg,200,(0.8,0.2,0.2)))
        print(siege())
        print(ligneTribune())
        print(cage())
        print(q)
        print(afficheQuilles(q,n))
        if len(q)==0 :
            write("Perdu ! C'est dommage :(")

