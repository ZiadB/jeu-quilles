

from random import*

def joueurJoue(q) :
    condition_i = len(q) + 1
    i= int(input("choisir une ligne : "))

    while i not in range(condition_i) :
        print("Erreur ! ligne saisie invalide")
        i=int(input("choisir une ligne : "))

    p = input("choisir une position (G, M, ou D) : ")
    while p not in "GMD" :
        print("Erreur ! positision invalide")
        p = input("choisir une position (G, M, ou D) : ")
        

    joueur = str(i) + ":" + p
    return joueur

def ordiJoue(q) :
    p = choice(["G","M","D"])
    i = randint(1, len(q))
    ordi = str(i) + ":" + p

    if p == "M" :
        print("L'ordinateur a joué au milieu sur la ligne",i," et a tiré 2 quilles")

    elif p == "G":
        print("L'ordinateur a joué à gauche sur la ligne",i," et a tiré 1 quille")
    else :
        print("L'ordinateur a joué à droite sur la ligne",i," et a tiré 1 quille")

    return ordi


def jouerMilieu (c, q) :
    ligne = int(c[0])
    a = ligne - 1
    nbQuilles = q[a][-1] + 1 - q[a][0]

    milieuPair = int(nbQuilles/2 - 2 + q[a][0])
    milieuImpair = int((nbQuilles + 1 )/2 - 2 + q[a][0])

    if nbQuilles % 2 == 0 :
        q[a][-1] = milieuPair
        if q[a][0] > q[a][1] :
            del q[a]
        else :
            q.insert(ligne, [milieuPair + 3, nbQuilles - 1 + q[a][0]])
    

    else :
        q[a][-1] = milieuImpair
        if q[a][0] > q[a][1] :
            del q[a]
        else :
            q.insert(ligne , [milieuImpair + 3, nbQuilles - 1 + q[a][0]])
     
    return q 
    

def jouerCote (c,q) :
    ligne = int(c[0])
    pos = c[2]
    a = ligne - 1
    
    if pos == "G" :
        q[a][0] += 1
        if q[a][0] > q[a][1] :
            del q[a]
        

    elif pos == "D" :
        q[a][-1] -= 1
        if q[a][0] > q[a][1] :
            del q[a]

    return q

def jouer(c,q) :

    if c[2] == "M" :
        jouerMilieu(c,q)
        

    elif c[2] == "G" or "D" :
        jouerCote(c,q)

    return q




def afficheQuilles(q, n):
    quilles=list("."*n)
    nbListe=len(q)
    
    for ligne in q:
        for i in range(ligne[0], ligne[1]+1):
            quilles[i] = "|"
            
    for k in range (len(q)) :
        if q[k][0] > q[k][1] :
            nbListe = nbListe - 1
            
    print("Voici les quilles, il y a ",nbListe, "lignes")
    return "" .join(quilles)  #commande .join trouvé grâce à l'aide d'un autre groupe




print("Bienvenue au Jeu des Quilles !")
n=randint(5,15)
q = [[0, n -1]]
print(afficheQuilles(q,n))

while len(q) != 0 :
    jouer(joueurJoue(q),q)
    print(afficheQuilles(q,n))
    if len(q) == 0 :
        print("Victoire ! C'est gagné :) ")
        break
    else :
        jouer(ordiJoue(q),q)
        print(afficheQuilles(q,n))
        if len(q)==0 :
            print("Perdu ! C'est dommage :(")



    


    
            




